package humanity.health.weatherapplication.cityweatheer.testuitls

import androidx.lifecycle.Observer
import humanity.health.sharedweathercity.baseviewmodel.CoroutineDispatcherProvider
import io.mockk.spyk
import kotlinx.coroutines.CoroutineDispatcher

class TestObserver<T> : Observer<T> {
    private val changedValues = mutableListOf<T?>()
    override fun onChanged(t: T?) {
        changedValues.add(t)
    }

    fun values(): List<T?> {
        return changedValues
    }

    fun assertContains(value: T?) {
        assert(changedValues.contains(value))
    }
}

fun <T> mockObserver(): Observer<T> {
    return spyk(TestObserver())
}

fun createTestDispatcherProvider(coroutineDispatcher: CoroutineDispatcher): CoroutineDispatcherProvider =
    object : CoroutineDispatcherProvider {
        override fun bgDispatcher() = coroutineDispatcher
        override fun ioDispatcher() = coroutineDispatcher
        override fun uiDispatcher() = coroutineDispatcher
    }

fun CoroutineDispatcher.asDispatcherProvider(): CoroutineDispatcherProvider =
    createTestDispatcherProvider(this)