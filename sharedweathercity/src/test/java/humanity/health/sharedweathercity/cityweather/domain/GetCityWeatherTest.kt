package humanity.health.sharedweathercity.cityweather.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import humanity.health.sharedweathercity.cityweather.data.*
import humanity.health.weatherapplication.cityweatheer.testuitls.MainCoroutineRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import kotlinx.coroutines.delay
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class GetCityWeatherTest{

    @get:Rule
    val instantTaskRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @RelaxedMockK
    lateinit var weatherAPI: WeatherAPI

    @RelaxedMockK
    private lateinit var mockedWeatherDT: CityWeatherDTO

    private fun createRemoteWeatherRepository() = RemoteWeatherRepository(weatherAPI = weatherAPI)

    @Test
    fun `when repository is called for getting a specific city weather in specific units,then it should call the api with proper value `() =
        mainCoroutineRule.runBlockingTest {
            coEvery {
                weatherAPI.getWeatherByCity(any(), any())
            } coAnswers {
                delay(100)
                mockedWeatherDT
            }
            val repository = createRemoteWeatherRepository()
            val testCityName = "khorramabad"
            val testUnit = "metric"
            repository.getCityWeather(testCityName, testUnit)
            delay(200)
            coVerify(exactly = 1) {
                weatherAPI.getWeatherByCity(testCityName, testUnit)
            }
        }


    @Test
    fun `when repository is called for getting a specific city weather in specific units,then it should call the api with proper mapped value from api `() {
        val fakeAPIResponse = CityWeatherDTO(
            coordinate = CoordinateDTO(1.0, 1.0),
            weather = listOf(
                WeatherDTO(
                    id = 0, main = "", description = "", iconId = ""
                )
            ),
            mainWeather = MainWeatherDTO(
                temp = 0.0,
                feelsLike = 0.0,
                tempMin = 0.0,
                tempMax = 0.0,
                pressure = 0,
                humidity = 0
            ),
            name = "khorramabad",
            id = 66,
            wind = WindDTO(deg = 1.0, speed = 1.0, gust = 1.0),
            clouds = CloudsDTO(all = 10),
            timeStamp = 1,
            weatherSystem = WeatherSystemDTO(
                country = "PL",
                sunrise = 1642142341,
                sunset = 1642171824
            ),
            timezone = -1
        )
        val expectedMappedValue = CityWeather(
            coordinate = Coordinate(1.0, 1.0),
            weather = listOf(
                Weather(
                    id = 0, main = "", description = "", iconId = ""
                )
            ),
            mainWeather = MainWeather(
                temp = 0.0,
                feelsLike = 0.0,
                tempMin = 0.0,
                tempMax = 0.0,
                pressure = 0,
                humidity = 0
            ),
            name = "khorramabad",
            id = 66,
            wind = Wind(deg = 1.0, speed = 1.0, gust = 1.0),
            clouds = Clouds(all = 10),
            timeStamp = 1,
            weatherSystem = WeatherSystem(
                country = "PL",
                sunrise = 1642142341,
                sunset = 1642171824
            ),
            timezone = -1
        )
        mainCoroutineRule.runBlockingTest {
            coEvery {
                weatherAPI.getWeatherByCity(any(), any())
            } coAnswers {
                delay(100)
                fakeAPIResponse
            }
            val repository = createRemoteWeatherRepository()
            val testCityName = "khorramabad"
            val testUnit = "metric"
            val returnedValue = repository.getCityWeather(testCityName, testUnit)
            delay(200)
            kotlin.test.assertEquals(expectedMappedValue, returnedValue)
        }
    }

}