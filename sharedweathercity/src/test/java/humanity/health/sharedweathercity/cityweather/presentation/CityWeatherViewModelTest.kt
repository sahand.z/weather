package humanity.health.sharedweathercity.cityweather.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import humanity.health.sharedweathercity.cityweather.domain.*
import humanity.health.weatherapplication.cityweatheer.testuitls.MainCoroutineRule
import humanity.health.weatherapplication.cityweatheer.testuitls.asDispatcherProvider
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import kotlinx.coroutines.delay
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class CityWeatherViewModelTest {
    @get:Rule
    val instantTaskRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @RelaxedMockK
    private lateinit var getWeatherUseCase: GetCityWeather

    @RelaxedMockK
    private lateinit var cityWeather: CityWeather

    private fun createViewModel() = CityWeatherViewModel(
        getCityWeather = getWeatherUseCase,
        coroutineDispatcherProvider = mainCoroutineRule.getDispatcher().asDispatcherProvider(),
    )

    @Test
    fun `when user requested a city to be queried, then viewModel must call useCase to load it`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()

            coEvery { getWeatherUseCase.execute(any(), any()) } coAnswers {
                delay(300)
                cityWeather
            }
            assertEquals(NotLoaded, viewModel.currentState.cityWeather)
            viewModel.getCityWeatherRequested()
            delay(100)
            coVerify(exactly = 1) { getWeatherUseCase.execute(any(), any()) }
        }

    @Test
    fun `when viewModel stats to load city weather, then viewModel state should be loading`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()
            coEvery { getWeatherUseCase.execute(any(), any()) } coAnswers {
                delay(300)
                cityWeather
            }
            viewModel.getCityWeatherRequested()
            delay(100)
            assertEquals(Loading, viewModel.currentState.cityWeather)
        }

    @Test
    fun `when viewModel failed to load a city's weather, then viewModel must update its state to Failed with proper exception`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()
            val exception = Exception("custom exception")
            coEvery { getWeatherUseCase.execute(any(), any()) } coAnswers {
                delay(100)
                throw exception
            }
            viewModel.getCityWeatherRequested()
            delay(200)
            assertEquals(Failed<java.lang.Exception>(exception), viewModel.currentState.cityWeather)
        }

    @Test
    fun `when viewModel loaded the requested city's weather, then viewModel must update its state to Loaded with proper value`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()
            coEvery { getWeatherUseCase.execute(any(), any()) } coAnswers {
                delay(300)
                cityWeather
            }
            viewModel.getCityWeatherRequested()
            delay(400)
            assertEquals(Loaded(cityWeather), viewModel.currentState.cityWeather)

        }

    @Test
    fun `when user changed the value of the query, then viewModel must store the query in its state`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()
            val mockQuery = "warsaw"
            viewModel.queryChanged(mockQuery)
            assertEquals(mockQuery, viewModel.currentState.cityQuery)
        }

    @Test
    fun `when user changes the unitSystem, then viewModel must update its state accordingly`() =
        mainCoroutineRule.runBlockingTest {
            val viewModel = createViewModel()
            val mockUnitSystem = UnitSystem.METRIC
            assertEquals(UnitSystem.METRIC, viewModel.currentState.selectedUnit)
            viewModel.unitsSystemChanged(mockUnitSystem)
            assertEquals(mockUnitSystem, viewModel.currentState.selectedUnit)
        }


}