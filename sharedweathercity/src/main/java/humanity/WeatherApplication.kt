package humanity

import android.app.Application
import humanity.health.sharedweathercity.di.networkModule
import humanity.health.weatherapplication.cityweatheer.cityWeatherModule
import org.koin.core.context.startKoin

class WeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(
                listOf(
                    networkModule,
                    cityWeatherModule,
                )
            )
        }
    }

}