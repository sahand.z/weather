package humanity.health.sharedweathercity.utils

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import androidx.annotation.AttrRes

fun Context.getColorFromTheme(@AttrRes attrRes: Int): Int = theme.getColorFromAttr(attrRes)

fun Resources.Theme.getColorFromAttr(@AttrRes attrRes: Int): Int {
    val typedValue = TypedValue()
    resolveAttribute(attrRes, typedValue, true)
    return typedValue.data
}