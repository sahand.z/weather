package humanity.health.sharedweathercity.utils

import android.view.View


/**This functions reject multiple clicks on a view in a shorrt period
 * We use this to prevent bug like double clicking on buttons with
 * navigation action which usually causes crash*/
class SafeViewClickListener(private val clickListener: ((view: View) -> Unit)?) : View.OnClickListener {
    private var isEnabled = true
    override fun onClick(v: View) {
        if (isEnabled) {
            isEnabled = false
            clickListener?.invoke(v)
            v.postDelayed(Runnable {
                isEnabled = true
            }, 500)
        }
    }
}