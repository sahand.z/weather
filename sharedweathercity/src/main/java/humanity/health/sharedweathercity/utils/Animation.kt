package humanity.health.sharedweathercity.utils

import android.animation.ValueAnimator
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator


/** This extension function animates the textview value from number Zero to the given value
 * */
fun TextView.animateDoubleAsString(value: Double, duration: Long = 500) {
    val numberAnimator = ValueAnimator.ofFloat(0f, value.toFloat())
    numberAnimator.interpolator = LinearOutSlowInInterpolator()
    numberAnimator.duration = duration
    numberAnimator.addUpdateListener { animator ->
        text =
            String.format("%.2f", animator.animatedValue)
    }
    numberAnimator.start()
}

/** This extension function animates the textview color from @param start to @param end
 * */
fun TextView.animateTextColor(startColor: Int, endColor: Int, duration: Long = 500) {
    val numberAnimator = ValueAnimator.ofInt(startColor, endColor)
    numberAnimator.duration = duration
    numberAnimator.addUpdateListener { animator ->
        setTextColor(animator.animatedValue as Int)
    }
    numberAnimator.start()
}

/** makes view smoothly visible
 * */
fun View.fadeInAndVisible(duration: Long = 300, initialize: Boolean = false) {
    if (initialize) {
        alpha = 0f
    }
    animate().alpha(1f).withStartAction {
        visible()
    }.setInterpolator(DecelerateInterpolator()).setDuration(duration).start()
}

/** makes view smoothly gone
 * */
fun View.fadeOutAndGone(duration: Long = 300) {
    animate().alpha(0f).withEndAction {
        gone()
    }.setInterpolator(DecelerateInterpolator()).setDuration(duration).start()
}

private fun View.visible() {
    visibility = View.VISIBLE
}

private fun View.gone() {
    visibility = View.GONE
}