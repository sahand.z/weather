package humanity.health.sharedweathercity.baseviewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(
    protected val coroutineContexts: CoroutineDispatcherProvider
) : ViewModel(), CoroutineScope {

    protected val job = SupervisorJob()

    protected val scope = CoroutineScope(mainDispatcher() + job)

    fun bgDispatcher(): CoroutineDispatcher {
        return coroutineContexts.ioDispatcher()
    }

    fun mainDispatcher(): CoroutineDispatcher {
        return coroutineContexts.uiDispatcher()
    }

    fun immediateDispatcher(): CoroutineDispatcher {
        return coroutineContexts.immediateDispatcher()
    }

    fun ioDispatcher(): CoroutineDispatcher {
        return coroutineContexts.ioDispatcher()
    }

    /** This method starts a coroutine on background dispatcher for CPU-Intensive coroutines*/
    suspend inline fun <T> onBg(crossinline coroutine: suspend () -> T): T {
        return withContext(bgDispatcher()) {
            coroutine()
        }
    }

    /** This method starts a coroutine on IO dispatcher for IO-Based coroutines*/
    suspend inline fun <T> onIO(crossinline coroutine: suspend () -> T): T {
        return withContext(ioDispatcher()) {
            coroutine()
        }
    }

    /** This method starts a coroutine on UI dispatcher for ui coroutines*/
    suspend inline fun <T> onUI(crossinline coroutine: suspend () -> T): T {
        return withContext(mainDispatcher()) {
            coroutine()
        }
    }

    /** This method starts a coroutine on UI dispatcher immediately */
    suspend inline fun <T> onUIImmediate(crossinline coroutine: suspend () -> T): T {
        return withContext(immediateDispatcher()) {
            coroutine()
        }
    }

    override val coroutineContext: CoroutineContext
        get() = scope.coroutineContext

    override fun onCleared() {
        super.onCleared()
        scope.coroutineContext.cancelChildren()
    }


}