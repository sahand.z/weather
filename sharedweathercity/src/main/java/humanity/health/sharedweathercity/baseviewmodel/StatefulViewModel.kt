package humanity.health.sharedweathercity.baseviewmodel

import androidx.annotation.VisibleForTesting
import androidx.annotation.VisibleForTesting.PROTECTED
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.runBlocking

/**
 ** @author Created by Sahand Zehtabchi - (sahand.zehtabchi@gmail.com)
 *
 * Use this class to store your View's state.
 * e.g:
 *
 * data class ProfileViewState(
 *         val profileDataViewState = ProfileDataViewState(),
 *         val profileUploadImageViewState = ProfileUploadImageViewState()
 * )
 *
 * data class ProfileDataViewState (val isLoading:Boolean = false,
 *                                  val profileImage: String? = null,
 *                                  val profileData: Profile? = null,
 *                                  val profileLoadError: Exception? = null)
 *
 * data class ProfileUploadImageViewState(
 *         val isLoading: Boolean = false,
 *         val error: Exception? = null,
 *         val uploadedImage:String? = null
 * )
 *
 * val profileState = ViewStateStore(ProfileViewState())
 */
abstract class StatefulViewModel<STATE : Any>(
    initialState: STATE,
    coroutineContexts: CoroutineDispatcherProvider,
) : BaseViewModel(coroutineContexts) {

    private val stateStore = ViewStateStore(initialState)

    val currentState: STATE
        get() {
            return stateStore.state
        }

    fun observe(owner: LifecycleOwner, observer: (STATE) -> Unit) =
        stateStore.observe(owner, observer)


    /**
     * Is called after the state is successfully updated
     */
    @VisibleForTesting(otherwise = PROTECTED)
    open fun onStateUpdated(currentState: STATE) {

    }

    /**
     * Used to update states outside of coroutines.
     * This method should only be called on the main thread since it is updating a LiveData.
     * If you are inside a coroutine and a context other than the main dispatcher, use onUI { applyState (...) }
     */
    @VisibleForTesting(otherwise = PROTECTED)
    fun applyState(function: STATE.() -> STATE) = runBlocking {
        return@runBlocking onUIImmediate {
            val oldState = stateStore.state
            val newState = function(oldState)
            if (newState == oldState)
                return@onUIImmediate // don't change the state, since we want distinct changes in the state

            stateStore.state = newState
            onStateUpdated(currentState)
        }
    }


}