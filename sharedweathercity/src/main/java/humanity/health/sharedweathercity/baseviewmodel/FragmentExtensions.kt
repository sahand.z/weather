package humanity.health.sharedweathercity.baseviewmodel

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.savedstate.SavedStateRegistryOwner
import org.koin.androidx.viewmodel.ViewModelParameter
import org.koin.androidx.viewmodel.koin.getViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.java.KoinJavaComponent.getKoin

/** This class creates a shared viewModel that will be kept in memory while
 * as far as the given nav graph is on screen*/
inline fun <reified VM : ViewModel> Fragment.navGraphViewModel(
    @IdRes navGraphId: Int,
    qualifier: Qualifier? = null,
    initialState: Bundle = Bundle(),
    registryOwner: SavedStateRegistryOwner? = null,
    noinline parameters: ParametersDefinition? = null,
) = lazy {
    val store = findNavController().getViewModelStoreOwner(navGraphId).viewModelStore
    getKoin().getViewModel(
        ViewModelParameter(
            VM::class,
            qualifier,
            parameters,
            initialState,
            store,
            registryOwner
        )
    )
}