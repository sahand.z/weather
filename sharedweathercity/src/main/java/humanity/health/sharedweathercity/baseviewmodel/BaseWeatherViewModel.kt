package humanity.health.sharedweathercity.baseviewmodel

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

abstract class BaseWeatherViewModel<State : Any>(
    initialState: State,
    coroutineDispatcherProvider: CoroutineDispatcherProvider = coroutineDispatcherProvider()
) :
    StatefulViewModel<State>(initialState = initialState,coroutineContexts = coroutineDispatcherProvider)

fun coroutineDispatcherProvider() = object : CoroutineDispatcherProvider {
    override fun bgDispatcher(): CoroutineDispatcher {
        return Dispatchers.Default
    }

    override fun ioDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    override fun uiDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main
    }

    override fun immediateDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main.immediate
    }
}
