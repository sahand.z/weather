package humanity.health.sharedweathercity.cityweather.data

import humanity.health.sharedweathercity.cityweather.data.CityWeatherDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {
    @GET("weather")
    suspend fun getWeatherByCity(
        @Query("q") cityName: String,
        @Query("units") units: String,
    ): CityWeatherDTO
}
