package humanity.health.sharedweathercity.cityweather.data

import humanity.health.sharedweathercity.cityweather.domain.CityWeather

interface WeatherRepository {
    suspend fun getCityWeather(cityName: String, units: String): CityWeather
}