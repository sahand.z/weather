package humanity.health.sharedweathercity.cityweather.presentation

import humanity.health.sharedweathercity.baseviewmodel.BaseWeatherViewModel
import humanity.health.sharedweathercity.baseviewmodel.CoroutineDispatcherProvider
import humanity.health.sharedweathercity.cityweather.domain.*
import kotlinx.coroutines.launch

class CityWeatherViewModel(
    private val getCityWeather: GetCityWeather,
    coroutineDispatcherProvider: CoroutineDispatcherProvider,
) : BaseWeatherViewModel<CityWeatherViewModel.State>(
    State(), coroutineDispatcherProvider
) {
    data class State(
        val cityQuery: String = "Warsaw",
        val selectedUnit: UnitSystem = UnitSystem.METRIC,
        val cityWeather: LoadableData<CityWeather> = NotLoaded,
    )

    init {
        getCityWeatherRequested()
    }

    fun getCityWeatherRequested() {
        applyState {
            copy(cityWeather = Loading)
        }
        launch {
            onIO {
                runCatching {
                    getCityWeather.execute(
                        cityName = currentState.cityQuery,
                        unitSystem = currentState.selectedUnit
                    )
                }
            }.fold(onSuccess = {
                applyState {
                    copy(cityWeather = Loaded(it))
                }
            }, onFailure = {
                applyState {
                    copy(cityWeather = Failed(it))
                }
            })
        }
    }

    fun unitsSystemChanged(unitSystem: UnitSystem) {
        if (unitSystem != currentState.selectedUnit) {
            applyState {
                copy(selectedUnit = unitSystem)
            }
            getCityWeatherRequested()
        }
    }

    fun queryChanged(query: String) {
        applyState {
            copy(cityQuery = query)
        }
    }
}