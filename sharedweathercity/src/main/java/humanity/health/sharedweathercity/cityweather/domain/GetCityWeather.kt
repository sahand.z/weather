package humanity.health.sharedweathercity.cityweather.domain

import humanity.health.sharedweathercity.cityweather.data.WeatherRepository

class GetCityWeather(private val remoteWeatherRepository: WeatherRepository) {

    suspend fun execute(cityName: String, unitSystem: UnitSystem): CityWeather =
        remoteWeatherRepository.getCityWeather(cityName, unitSystem.unit)

}