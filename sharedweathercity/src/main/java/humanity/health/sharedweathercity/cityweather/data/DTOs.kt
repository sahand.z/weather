package humanity.health.sharedweathercity.cityweather.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/** We make all the CityWeather nullable mandatory
 * as they might not be available from api
 * @see <a href="https://openweathermap.org/current">openweathermap</a>
 */
@Serializable
data class CityWeatherDTO(
    @SerialName("coord")
    val coordinate: CoordinateDTO? = null,
    @SerialName("weather")
    val weather: List<WeatherDTO>? = null,
    @SerialName("main")
    val mainWeather: MainWeatherDTO? = null,
    @SerialName("name")
    val name: String? = null,
    @SerialName("id")
    val id: Long? = null,
    @SerialName("wind")
    val wind: WindDTO? = null,
    @SerialName("clouds")
    val clouds: CloudsDTO? = null,
    @SerialName("timeStamp")
    val timeStamp: Long? = null,
    @SerialName("weatherSystem")
    val weatherSystem: WeatherSystemDTO? = null,
    @SerialName("timezone")
    val timezone: Long? = null,
)

@Serializable
data class WeatherDTO(
    @SerialName("id") val id: Long? = null,
    @SerialName("main") val main: String? = null,
    @SerialName("description") val description: String? = null,
    @SerialName("icon") val iconId: String? = null,
)

@Serializable
data class MainWeatherDTO(
    @SerialName("temp") val temp: Double? = null,
    @SerialName("feels_like") val feelsLike: Double? = null,
    @SerialName("temp_min") val tempMin: Double? = null,
    @SerialName("temp_max") val tempMax: Double? = null,
    @SerialName("pressure") val pressure: Int? = null,
    @SerialName("humidity") val humidity: Int? = null,
)

@Serializable
data class WindDTO(
    @SerialName("deg") val deg: Double? = null,
    @SerialName("speed") val speed: Double? = null,
    @SerialName("gust") val gust: Double? = null,
)

@Serializable
data class CloudsDTO(@SerialName("all") val all: Long)

@Serializable
data class WeatherSystemDTO(
    @SerialName("country") val country: String? = null,
    @SerialName("sunrise") val sunrise: Long? = null,
    @SerialName("sunset") val sunset: Long? = null,
)

@Serializable
data class CoordinateDTO(
    @SerialName("lon") val longitude: Double,
    @SerialName("lat") val latitude: Double,
)

