package humanity.health.sharedweathercity.cityweather.data

import humanity.health.sharedweathercity.cityweather.domain.CityWeather

class RemoteWeatherRepository(private val weatherAPI: WeatherAPI): WeatherRepository {

    override suspend fun getCityWeather(cityName: String, units: String): CityWeather =
        weatherAPI.getWeatherByCity(cityName, units).toCityWeather()

}