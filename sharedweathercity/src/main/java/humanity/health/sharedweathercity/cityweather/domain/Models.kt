package humanity.health.sharedweathercity.cityweather.domain

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.Keep

enum class UnitSystem(val unit: String) {
    STANDARD("standard"), METRIC("metric"), IMPERIAL("imperial"),
}

/**We make all the CityWeather nullable mandatory
 *  as they might not be available from our data layer*/
@Keep
data class CityWeather(
    val coordinate: Coordinate? = null,
    val weather: List<Weather>? = null,
    val mainWeather: MainWeather? = null,
    val name: String? = null,
    val id: Long? = null,
    val wind: Wind? = null,
    val clouds: Clouds? = null,
    val timeStamp: Long? = null,
    val weatherSystem: WeatherSystem? = null,
    val timezone: Long? = null,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Coordinate::class.java.classLoader),
        parcel.createTypedArrayList(Weather),
        parcel.readParcelable(MainWeather::class.java.classLoader),
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readParcelable(Wind::class.java.classLoader),
        parcel.readParcelable(Clouds::class.java.classLoader),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readParcelable(WeatherSystem::class.java.classLoader),
        parcel.readValue(Long::class.java.classLoader) as? Long
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(coordinate, flags)
        parcel.writeTypedList(weather)
        parcel.writeParcelable(mainWeather, flags)
        parcel.writeString(name)
        parcel.writeValue(id)
        parcel.writeParcelable(wind, flags)
        parcel.writeParcelable(clouds, flags)
        parcel.writeValue(timeStamp)
        parcel.writeParcelable(weatherSystem, flags)
        parcel.writeValue(timezone)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CityWeather> {
        override fun createFromParcel(parcel: Parcel): CityWeather {
            return CityWeather(parcel)
        }

        override fun newArray(size: Int): Array<CityWeather?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class Weather(
    val id: Long? = null,
    val main: String? = null,
    val description: String? = null,
    val iconId: String? = null,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(main)
        parcel.writeString(description)
        parcel.writeString(iconId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Weather> {
        override fun createFromParcel(parcel: Parcel): Weather {
            return Weather(parcel)
        }

        override fun newArray(size: Int): Array<Weather?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class MainWeather(
    val temp: Double? = null,
    val feelsLike: Double? = null,
    val tempMin: Double? = null,
    val tempMax: Double? = null,
    val pressure: Int? = null,
    val humidity: Int? = null,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(temp)
        parcel.writeValue(feelsLike)
        parcel.writeValue(tempMin)
        parcel.writeValue(tempMax)
        parcel.writeValue(pressure)
        parcel.writeValue(humidity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MainWeather> {
        override fun createFromParcel(parcel: Parcel): MainWeather {
            return MainWeather(parcel)
        }

        override fun newArray(size: Int): Array<MainWeather?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class Wind(val deg: Double? = null, val speed: Double? = null, val gust: Double? = null) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(deg)
        parcel.writeValue(speed)
        parcel.writeValue(gust)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Wind> {
        override fun createFromParcel(parcel: Parcel): Wind {
            return Wind(parcel)
        }

        override fun newArray(size: Int): Array<Wind?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class Clouds(val all: Long? = null) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readValue(Long::class.java.classLoader) as? Long) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(all)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Clouds> {
        override fun createFromParcel(parcel: Parcel): Clouds {
            return Clouds(parcel)
        }

        override fun newArray(size: Int): Array<Clouds?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class WeatherSystem(
    val country: String? = null,
    val sunrise: Long? = null,
    val sunset: Long? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Long::class.java.classLoader) as? Long
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeValue(sunrise)
        parcel.writeValue(sunset)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherSystem> {
        override fun createFromParcel(parcel: Parcel): WeatherSystem {
            return WeatherSystem(parcel)
        }

        override fun newArray(size: Int): Array<WeatherSystem?> {
            return arrayOfNulls(size)
        }
    }
}

@Keep
data class Coordinate(val longitude: Double, val latitude: Double) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(longitude)
        parcel.writeDouble(latitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Coordinate> {
        override fun createFromParcel(parcel: Parcel): Coordinate {
            return Coordinate(parcel)
        }

        override fun newArray(size: Int): Array<Coordinate?> {
            return arrayOfNulls(size)
        }
    }
}

