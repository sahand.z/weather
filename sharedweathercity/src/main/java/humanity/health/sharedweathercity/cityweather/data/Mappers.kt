package humanity.health.sharedweathercity.cityweather.data

import humanity.health.sharedweathercity.cityweather.domain.*


fun CityWeatherDTO.toCityWeather() = CityWeather(
    coordinate = coordinate?.let { Coordinate(longitude = it.longitude, latitude = it.latitude) },
    weather = weather?.map {
        Weather(
            id = it.id,
            main = it.main,
            description = it.description,
            iconId = it.iconId
        )
    },
    mainWeather = mainWeather?.let { MainWeather(
        temp = it.temp,
        feelsLike = it.feelsLike,
        tempMin = it.tempMin,
        tempMax = it.tempMax,
        pressure = it.pressure,
        humidity = it.humidity,
    ) },
    name = name,
    id = id,
    wind = wind.let { Wind(deg = it?.deg, speed = it?.speed, gust = it?.gust) },
    clouds = Clouds(clouds?.all),
    timeStamp = timeStamp,
    weatherSystem = weatherSystem.let { WeatherSystem(country = it?.country, sunrise = it?.sunrise, sunset = it?.sunset) },
    timezone = timezone,
)