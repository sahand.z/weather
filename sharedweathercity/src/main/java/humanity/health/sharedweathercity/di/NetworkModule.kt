package humanity.health.sharedweathercity.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

private const val CONNECT_TIMEOUT = 15L
private const val WRITE_TIMEOUT = 15L
private const val READ_TIMEOUT = 15L
val networkModule = module {

    single(named("logger")) {
        getLogger()
    }

    single(named("apiKeyInterceptor")) {
        apiKeyInterceptor()
    }

    single {
        retrofitHttpClient()
    }
    single {
        retrofitBuilder()
    }

    single {
        Json { ignoreUnknownKeys = true }
    }

}

private fun apiKeyInterceptor(): Interceptor =
    Interceptor { chain ->
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url()
        val newUrl =
            originalUrl.newBuilder().addQueryParameter("appid", "8b7c1bfd3e00839d31bbbab349b80540")
                .build()
        val newRequest = originalRequest.newBuilder().url(newUrl).build()
        chain.proceed(newRequest)
    }


private fun getLogger(): Interceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BODY
    return logger
}


@OptIn(ExperimentalSerializationApi::class)
private fun Scope.retrofitBuilder(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org/data/2.5/")
        .addConverterFactory(get<Json>().asConverterFactory(MediaType.get("application/json")))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(get())
        .build()
}

private fun Scope.retrofitHttpClient(): OkHttpClient {
    return OkHttpClient.Builder().apply {
        connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        retryOnConnectionFailure(false)
        addInterceptor(get(named("logger")))
        addInterceptor(get(named("apiKeyInterceptor")))
    }.build()
}
