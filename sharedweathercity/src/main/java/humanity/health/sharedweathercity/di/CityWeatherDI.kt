package humanity.health.weatherapplication.cityweatheer

import humanity.health.sharedweathercity.baseviewmodel.coroutineDispatcherProvider
import humanity.health.sharedweathercity.cityweather.data.RemoteWeatherRepository
import humanity.health.sharedweathercity.cityweather.data.WeatherAPI
import humanity.health.sharedweathercity.cityweather.data.WeatherRepository
import humanity.health.sharedweathercity.cityweather.domain.GetCityWeather
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val cityWeatherModule = module {

    single {
        GetCityWeather(get())
    }

    viewModel {
        CityWeatherViewModel(get(),get())
    }

    single<WeatherRepository> {
        RemoteWeatherRepository(get())
    }

    single<WeatherAPI> {
        get<Retrofit>().create(WeatherAPI::class.java)
    }

    single {
        coroutineDispatcherProvider()
    }
}