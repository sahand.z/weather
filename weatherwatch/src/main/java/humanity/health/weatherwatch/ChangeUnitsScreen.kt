package humanity.health.weatherwatch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import humanity.health.sharedweathercity.baseviewmodel.navGraphViewModel
import humanity.health.sharedweathercity.cityweather.domain.UnitSystem
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import humanity.health.weatherwatch.databinding.WatchUnitChangeScreenBinding

class ChangeUnitsScreen: Fragment() {

    private val cityWeatherViewModel: CityWeatherViewModel by navGraphViewModel(R.id.watch_nav_graph_xml)

    private var _binding : WatchUnitChangeScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = WatchUnitChangeScreenBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(cityWeatherViewModel.currentState.selectedUnit){
            UnitSystem.METRIC -> {binding.changeUnitScreenCelsiusRadioButton.isChecked = true}
            UnitSystem.STANDARD -> {}
            UnitSystem.IMPERIAL -> {binding.changeUnitScreenFahrenheitRadioButton.isChecked = true}
        }

        binding.changeUnitScreenCelsiusRadioButton.setOnClickListener {
            cityWeatherViewModel.unitsSystemChanged(UnitSystem.METRIC)
            findNavController().navigateUp()
        }

        binding.changeUnitScreenFahrenheitRadioButton.setOnClickListener {
            cityWeatherViewModel.unitsSystemChanged(UnitSystem.IMPERIAL)
            findNavController().navigateUp()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}