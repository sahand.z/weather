package humanity.health.weatherwatch

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import humanity.health.weatherwatch.databinding.ActivityWatchBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class WatchActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWatchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWatchBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}