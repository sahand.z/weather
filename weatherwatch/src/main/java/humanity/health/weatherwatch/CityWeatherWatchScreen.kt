package humanity.health.weatherwatch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import humanity.health.sharedweathercity.baseviewmodel.navGraphViewModel
import humanity.health.sharedweathercity.cityweather.domain.*
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import humanity.health.sharedweathercity.utils.*
import humanity.health.weatherwatch.databinding.WatchWeatherCityBinding

class CityWeatherWatchScreen: Fragment() {
    private val cityWeatherViewModel: CityWeatherViewModel by navGraphViewModel(R.id.watch_nav_graph_xml)
    private var _binding: WatchWeatherCityBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = WatchWeatherCityBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityWeatherViewModel.observe(viewLifecycleOwner) {
            when (val cityWeather = it.cityWeather) {
                is Failed -> {
                    stopLoading()
                    clearCurrentViews()
                    showError()
                }
                is Loaded -> {
                    stopLoading()
                    showWeatherData(cityWeather.data)
                }
                Loading -> {
                    showLoading()
                }
                NotLoaded -> {

                }
            }

        }
        binding.watchActivityChangeUnitButton.setOnClickListener(
            SafeViewClickListener {
                findNavController().navigate(CityWeatherWatchScreenDirections.actionCityWeatherWatchScreenToChangeUnitsScreen())
            })
    }

    private fun showWeatherData(cityWeather: CityWeather) {
        binding.watchActivitySwipeToRefresh.setOnRefreshListener {
            clearCurrentViews()
            cityWeatherViewModel.getCityWeatherRequested()
        }
        with(cityWeather) {
            mainWeather?.temp?.let { temp ->
                binding.watchActivityCityTemp.animateDoubleAsString(temp, 200)
                name?.let { binding.watchActivityCityName.text = it }
                context?.let {
                    binding.watchActivityCityTemp.animateTextColor(
                        humanity.health.sharedweathercity.R.color.white,
                        humanity.health.sharedweathercity.R.color.black
                    )
                    binding.watchActivityCityName.animateTextColor(
                        humanity.health.sharedweathercity.R.color.white,
                        humanity.health.sharedweathercity.R.color.black
                    )
                }
            }
        }
    }

    private fun clearCurrentViews() {
        binding.watchActivityCityTemp.text =
            getString(humanity.health.sharedweathercity.R.string.defaultWeatherDegree)
        binding.watchActivityCityName.fadeOutAndGone()
        context?.let {
            binding.watchActivityCityName.fadeInAndVisible()
            binding.watchActivityCityTemp.animateTextColor(
                humanity.health.sharedweathercity.R.color.black,
                humanity.health.sharedweathercity.R.color.white,
            )
            binding.watchActivityCityName.animateTextColor(
                humanity.health.sharedweathercity.R.color.black,
                humanity.health.sharedweathercity.R.color.white,
            )
        }
    }

    private fun showError() {
        Toast.makeText(context,humanity.health.sharedweathercity.R.string.error_occurred, Toast.LENGTH_LONG).show()
    }

    private fun stopLoading() {
        binding.watchActivitySwipeToRefresh.isRefreshing = false
    }

    private fun showLoading() {
        binding.watchActivitySwipeToRefresh.isRefreshing = true
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}