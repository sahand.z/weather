package humanity.health.cityweather

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}