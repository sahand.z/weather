package humanity.health.cityweather.weather

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import humanity.health.cityweather.R
import humanity.health.cityweather.databinding.ChangeUnitBottomsheetBinding
import humanity.health.sharedweathercity.baseviewmodel.navGraphViewModel
import humanity.health.sharedweathercity.cityweather.domain.UnitSystem
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel

class ChangeUnitsBottomSheetDialog : BottomSheetDialogFragment() {
    private var _binding: ChangeUnitBottomsheetBinding? = null
    private val binding get() = _binding!!
    private val cityWeatherViewModel: CityWeatherViewModel by navGraphViewModel(R.id.main_nav_graph_xml)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ChangeUnitBottomsheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.changeUnitCelsiusRadioButton.isSelected = true

        when(cityWeatherViewModel.currentState.selectedUnit){
            UnitSystem.STANDARD -> {}
            UnitSystem.METRIC -> {
                binding.changeUnitCelsiusRadioButton.isChecked = true
            }
            UnitSystem.IMPERIAL -> {
                binding.changeUnitFahrenheitRadioButton.isChecked = true

            }
        }

        binding.changeUnitCelsiusRadioButton.setOnClickListener {
            cityWeatherViewModel.unitsSystemChanged(UnitSystem.METRIC)
            this@ChangeUnitsBottomSheetDialog.dismiss()
        }

        binding.changeUnitFahrenheitRadioButton.setOnClickListener {
            cityWeatherViewModel.unitsSystemChanged(UnitSystem.IMPERIAL)
            this@ChangeUnitsBottomSheetDialog.dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), R.style.RoundedBottomSheetDialog)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}