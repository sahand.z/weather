package humanity.health.cityweather.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import humanity.health.cityweather.R
import humanity.health.cityweather.databinding.WeatherScreenBinding
import humanity.health.sharedweathercity.baseviewmodel.navGraphViewModel
import humanity.health.sharedweathercity.cityweather.domain.*
import humanity.health.sharedweathercity.cityweather.presentation.CityWeatherViewModel
import humanity.health.sharedweathercity.utils.SafeViewClickListener
import humanity.health.sharedweathercity.utils.animateDoubleAsString
import humanity.health.sharedweathercity.utils.animateTextColor
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class CityWeatherScreen: Fragment() {

    private val cityWeatherViewModel: CityWeatherViewModel by navGraphViewModel(R.id.main_nav_graph_xml)
    private var _binding: WeatherScreenBinding? = null
    private val binding get() = _binding!!
    private lateinit var mapView: MapView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadMapConfig()
        _binding = WeatherScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = binding.cityWeatherDetailsMap
        clearCurrentViews()
        binding.cityWeatherSwipeToRefresh.setOnRefreshListener {
            clearCurrentViews()
            cityWeatherViewModel.getCityWeatherRequested()
        }
        cityWeatherViewModel.observe(viewLifecycleOwner) {
            when (val cityWeather = it.cityWeather) {
                is Failed -> {
                    stopLoading()
                    showError()
                }
                is Loaded -> {
                    stopLoading()
                    showWeatherData(cityWeather.data, cityWeatherViewModel.currentState.selectedUnit)
                }
                Loading -> {
                    showLoading()
                }
                NotLoaded -> {
                    stopLoading()
                }
            }
        }
        binding.cityWeatherDetailsChangeUnitButton.setOnClickListener(SafeViewClickListener {
            findNavController().navigate(CityWeatherScreenDirections.actionCityWeatherScreenToChangeUnitsBottomSheetDialog())
        })

    }

    override fun onResume() {
        super.onResume()
        loadMapConfig()
        if (mapView != null)
            mapView.onResume()
    }

    private fun clearCurrentViews() {
        binding.cityWeatherDetailsDegreeText.text =
            getString(humanity.health.sharedweathercity.R.string.defaultWeatherDegree)
        binding.cityWeatherDetailsCityText.text = ""
        binding.cityWeatherDetailsDegreeUnitText.text = ""
        binding.cityWeatherDetailsDegreeText.animateTextColor(
            humanity.health.sharedweathercity.R.color.black,
            humanity.health.sharedweathercity.R.color.white,
        )
        binding.cityWeatherDetailsCityText.animateTextColor(
            humanity.health.sharedweathercity.R.color.black,
            humanity.health.sharedweathercity.R.color.white,
        )
        binding.cityWeatherDetailsDegreeSeparatorText.animateTextColor(
            humanity.health.sharedweathercity.R.color.black,
            humanity.health.sharedweathercity.R.color.white,
        )
        binding.cityWeatherDetailsFeelsLikeText.animateTextColor(
            humanity.health.sharedweathercity.R.color.black,
            humanity.health.sharedweathercity.R.color.white,
        )
        binding.cityWeatherMainText.animateTextColor(
            humanity.health.sharedweathercity.R.color.white,
            humanity.health.sharedweathercity.R.color.black,
        )
        binding.cityWeatherDetailsDegreeUnitText.animateTextColor(
            humanity.health.sharedweathercity.R.color.black,
            humanity.health.sharedweathercity.R.color.white,
        )

    }

    private fun showError() {
        view?.let {
            Snackbar.make(it,humanity.health.sharedweathercity.R.string.error_occurred,3_000)
                .show()
        }
    }

    private fun showWeatherData(
        cityWeather: CityWeather, unit: UnitSystem
    ) {
        with(cityWeather) {
            showMap(coordinate, name)
            mainWeather?.temp?.let { temp ->
                binding.cityWeatherDetailsDegreeText.animateDoubleAsString(temp, 200)
                binding.cityWeatherDetailsDegreeUnitText.text =
                    if (unit == UnitSystem.METRIC) getString(
                        humanity.health.sharedweathercity.R.string.celsius
                    ) else getString(humanity.health.sharedweathercity.R.string.fahrenheit)
            }
            name?.let { binding.cityWeatherDetailsCityText.text = it }
            mainWeather?.feelsLike?.let {
                binding.cityWeatherDetailsFeelsLikeText.text =
                    getString(
                        humanity.health.sharedweathercity.R.string.feels_like_formatted,
                        it.toString()
                    )
            }

            weather?.first()?.main.let {
                binding.cityWeatherMainText.text =
                    it.toString()
            }
            binding.cityWeatherDetailsDegreeText.animateTextColor(
                humanity.health.sharedweathercity.R.color.white,
                humanity.health.sharedweathercity.R.color.black,
            )
            binding.cityWeatherDetailsCityText.animateTextColor(
                humanity.health.sharedweathercity.R.color.white,
                humanity.health.sharedweathercity.R.color.black,
            )
            binding.cityWeatherDetailsDegreeSeparatorText.animateTextColor(
                humanity.health.sharedweathercity.R.color.white,
                humanity.health.sharedweathercity.R.color.black,
            )
            binding.cityWeatherDetailsFeelsLikeText.animateTextColor(
                humanity.health.sharedweathercity.R.color.white,
                humanity.health.sharedweathercity.R.color.black,
            )

            binding.cityWeatherMainText.animateTextColor(
                humanity.health.sharedweathercity.R.color.black,
                humanity.health.sharedweathercity.R.color.white,
            )
            binding.cityWeatherDetailsDegreeUnitText.animateTextColor(
                humanity.health.sharedweathercity.R.color.white,
                humanity.health.sharedweathercity.R.color.black,
            )
        }
    }

    private fun loadMapConfig() {
        context?.let {
            Configuration.getInstance().load(it, PreferenceManager.getDefaultSharedPreferences(it))
        }
    }

    private fun showMap(coordinate: Coordinate?, cityName: String?) {
        coordinate?.apply {
            mapView.apply {
                val startPoint = GeoPoint(latitude, longitude)
                setTileSource(TileSourceFactory.MAPNIK)
                setMultiTouchControls(true)
                val mapController: IMapController = controller
                mapController.setZoom(10.0)
                mapController.setCenter(startPoint)
                val marker = Marker(mapView)
                marker.position = startPoint
                marker.setTextIcon(cityName ?: "")
                marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                mapView.overlays.add(marker)
                mapView.invalidate()
                mapView.overlays.add(marker)
            }
        }
    }

    private fun stopLoading() {
        binding.cityWeatherSwipeToRefresh.isRefreshing = false
        binding.cityWeatherDetailsLayout.transitionToStart()
    }

    private fun showLoading() {
        binding.cityWeatherSwipeToRefresh.isRefreshing = true
        binding.cityWeatherDetailsLayout.transitionToEnd()
        binding.cityWeatherDetailsLayout.setProgress(1f, 1f)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}